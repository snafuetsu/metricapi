﻿using System;
using Xunit;
using MetricAPI.Services;
using MetricAPI.Controllers;
using MetricAPI.Models;
//using XUnit_Tests_MetricAPI.FakeServices;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using FakeItEasy;

namespace XUnit_Tests_MetricAPI
{
    public class LogProjectControllerTest
    {
        //Fakes
        private readonly ILogProjectService _service;

        //Dummy Data Generator
        private readonly Fixture _fixture;

        //System under test
        private readonly LogProjectController _controller;
        public LogProjectControllerTest()
        {
            _service = A.Fake<ILogProjectService>();
            _controller = new LogProjectController(_service);

            _fixture = new Fixture();
        }

        [Fact]
        public void Get_ValidIdStringPassed_ReturnsLogProject()
        {
            // Arrange
            var logProject = _fixture.Create<LogProject>();
            var selector = logProject.Id;
            A.CallTo(() => _service.Get(selector)).Returns(logProject);

            // Act
            var logProjectReturn = _controller.Get(selector);

            // Assert
            Assert.IsType<ActionResult<LogProject>>(logProjectReturn);
        }

        [Fact]
        public void Get_InvalidIdStringPassed_ReturnsNotFound()
        {
            // Arrange
            var logProject = _fixture.Create<LogProject>();
            var selector = "rgb";
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var notFoundReturn = _controller.Get(selector).Result;

            // Assert
            Assert.IsType<NotFoundResult>(notFoundReturn);
        }

        [Fact]
        public void Create_PassedValidLogProject_ReturnsOkResult()
        {
            // Arrange
            var entry = _fixture.Create<LogProject>();

            //Act
            var okResult = _controller.Create(entry).Result;

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void Update_PassedValidParams_ReturnsNoContentResult()
        {
            // Arrange
            var logProject = _fixture.Create<LogProject>();
            var selector = logProject.Id;
            var newLogProject = _fixture.Create<LogProject>();
            A.CallTo(() => _service.Get(selector)).Returns(logProject);

            // Act
            var noContentResult = _controller.Update(logProject.Id, newLogProject).Result;

            // Assert
            Assert.IsType<NoContentResult>(noContentResult);
        }

        [Fact]
        public void Update_PassedValidParams_ConfirmsUpdateMade()
        {
            // Arrange
            var logProject = _fixture.Create<LogProject>();
            var selector = logProject.Id;
            var newLogProject = _fixture.Create<LogProject>();
            A.CallTo(() => _service.Get(selector)).Returns(logProject);

            // Act
            var actionResponse = _controller.Update(logProject.Id, newLogProject);
            var updatedLogProject = _controller.Get(logProject.Id);

            // Assert
            Assert.Equal((updatedLogProject.Value as LogProject).Description, logProject.Description);
        }

        [Fact]
        public void Update_InvalidParamsPassed_ReturnsNotFound()
        {
            // Arrange
            var logProject = _fixture.Create<LogProject>();
            var selector = "77";
            var newLogProject = _fixture.Create<LogProject>();
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var notFoundResult = _controller.Update(selector, newLogProject).Result;

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void Delete_ValidIdStringPassed_ReturnsNoContentResult()
        {
            // Arrange
            var logProject = _fixture.Create<LogProject>();
            var selector = logProject.Id;
            A.CallTo(() => _service.Get(selector)).Returns(logProject);

            // Act
            var actionResult = _controller.Delete(selector);

            // Assert
            Assert.IsType<NoContentResult>(actionResult);
        }

        [Fact]
        public void Delete_InvalidStringIdPassed_ReturnsNotFound()
        {
            // Arrange 
            var logProject = _fixture.Create<LogProject>();
            var selector = "77";
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var actionResult = _controller.Delete(selector);

            // Assert
            Assert.IsType<NotFoundResult>(actionResult);
        }
    }
}
