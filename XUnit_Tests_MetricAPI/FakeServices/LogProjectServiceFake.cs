﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetricAPI.Models;
using MetricAPI.Services;

namespace XUnit_Tests_MetricAPI.FakeServices
{
    public class LogProjectServiceFake : ILogProjectService
    {
        private readonly List<LogProject> _logProjects;

        public LogProjectServiceFake()
        {
            _logProjects = new List<LogProject>()
            {
                new LogProject() { Id = "1", Name = "Test Project 1", Description = "This is the first test project."},
                new LogProject() { Id = "2", Name = "Test Project 2", Description = "This is the second test project."},
                new LogProject() { Id = "3", Name = "Test Project 3", Description = "This is the third test project."},
                new LogProject() { Id = "4", Name = "Test Project 4", Description = "This is the final test project."}
            };
        }


        
        public List<LogProject> Get(int page, int pagesize, out int totalresults)
        {
            /*var fluentfind = _logProjects.Find(logProject => true);
            totalresults = (int)fluentfind.CountDocuments();
            return fluentfind
                    .Skip((page - 1) * pagesize)
                    .Limit(pagesize)
                    .ToList();*/

            throw new NotImplementedException();
        }
        

        public LogProject Get(string id)
        {
            return _logProjects.Where(a => a.Id == id).FirstOrDefault();
        }

        public LogProject Create(LogProject logProject)
        {
            _logProjects.Add(logProject);
            return logProject;
        }

        public void Update(string id, LogProject logProjectIn)
        {
            var current = _logProjects.First(a => a.Id == id);
            var locale = _logProjects.IndexOf(current);
            _logProjects[locale] = logProjectIn;
        }

        public void Remove(LogProject logProjectIn)
        {
            var exists = _logProjects.First(a => a.Id == logProjectIn.Id);
            _logProjects.Remove(exists);
        }

        public void Remove(string id)
        {
            var exists = _logProjects.First(a => a.Id == id);
            _logProjects.Remove(exists);
        }
    }
}
