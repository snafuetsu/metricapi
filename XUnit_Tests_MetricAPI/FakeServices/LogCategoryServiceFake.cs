﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetricAPI.Models;
using MetricAPI.Services;


namespace XUnit_MetricAPI_Controller_Tests.FakeServices
{
    public class LogCategoryServiceFake : ILogCategoryService
    {
        private readonly List<LogCategory> _logCategories;

        public LogCategoryServiceFake()
        {

            _logCategories = new List<LogCategory>() { 
                new LogCategory { Id = "0", Name = "Category 1", Description = "This is the first category", Project = "Big Balls o' Fire", Units = "NFG"},
                new LogCategory { Id = "1", Name = "Category 6", Description = "This is the sixth category", Project = "Big Balls o' Fire", Units = "eek"},
                new LogCategory { Id = "2", Name = "Category 2", Description = "This is the second category", Project = "stonks", Units = "bondulance"},
                new LogCategory { Id = "3", Name = "Category 45", Description = "This is the forty-fifth category", Project = "YEET", Units = "kobe"}

            };
        }

        //Paginitation not being tested at 3am
        
        public List<LogCategory> Get(string projectid, int page, int pagesize, out int totalresults)
        {
            var query = from c in _logCategories.AsQueryable<LogCategory>()
                        where c.Project == projectid
                        select c;

            totalresults = query.Count();
            return query.Skip((page - 1) * pagesize).Take(pagesize).ToList();
        }
        

        public LogCategory Get(string id)
        {
            return _logCategories.Where(logCategory => logCategory.Id == id).FirstOrDefault();
        }

        public LogCategory Create(LogCategory logCategory)
        {
            _logCategories.Add(logCategory);
            return logCategory;
        }

        public void Update(string id, LogCategory logCategoryIn)
        {
            var current = _logCategories.First(a => a.Id == id);
            var locale = _logCategories.IndexOf(current);
            _logCategories[locale] = logCategoryIn;
        }

        public void Remove(LogCategory logCategoryIn)
        {
            var exists = _logCategories.First(a => a.Id == logCategoryIn.Id);
            _logCategories.Remove(exists);
        }

        public void Remove(string id)
        {
            var exists = _logCategories.First(a => a.Id == id);
            _logCategories.Remove(exists);
        }
    }
}
