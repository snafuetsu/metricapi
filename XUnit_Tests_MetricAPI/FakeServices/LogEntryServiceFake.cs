﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetricAPI.Models;
using MetricAPI.Services;

namespace XUnit_MetricAPI_Controller_Tests.FakeServices
{
    public class LogEntryServiceFake : ILogEntryService
    {
        private readonly List<LogEntry> _logEntries;
        private readonly List<LogCategory> _logCategories;

        public LogEntryServiceFake()
        {
            
            _logEntries = new List<LogEntry>()
            {
                new LogEntry() { Category="0", Id="1", Timestamp= new DateTime(2019, 11, 18), Value=1},
                new LogEntry() { Category="0", Id="2", Timestamp= new DateTime(2019, 11, 15), Value=4},
                new LogEntry() { Category="1", Id="1", Timestamp= new DateTime(2019, 11, 15), Value=3},
                new LogEntry() { Category="4", Id="3", Timestamp= new DateTime(2019, 11, 14), Value=1}
            };

            _logCategories = new List<LogCategory>() 
            {
                new LogCategory { Id = "0", Name = "Category 1", Description = "This is the first category", Project = "Big Balls o' Fire", Units = "NFG"},
                new LogCategory { Id = "1", Name = "Category 6", Description = "This is the sixth category", Project = "Big Balls o' Fire", Units = "eek"},
                new LogCategory { Id = "2", Name = "Category 2", Description = "This is the second category", Project = "stonks", Units = "bondulance"},
                new LogCategory { Id = "3", Name = "Category 45", Description = "This is the forty-fifth category", Project = "YEET", Units = "kobe"}

            };
        }

        public List<LogEntry> Get(string categoryid, int page, int pagesize, DateTime? start, DateTime? end, out int totalresults)
        {
            var query = from e in _logEntries.AsQueryable<LogEntry>()
                        where e.Category == categoryid
                        select e;

            if (start != null)
                query = from e in query
                        where e.Timestamp >= start
                        select e;

            if (end != null)
                query = from e in query
                        where e.Timestamp <= end
                        select e;

            totalresults = query.Count();

            return query.Skip((page - 1) * pagesize).Take(pagesize).ToList<LogEntry>();
        }

        public LogEntry Get(string id) =>
            _logEntries.Find<LogEntry>(logEntry => logEntry.Id == id).FirstOrDefault();

        public LogEntry Create(LogEntry logEntry)
        {
            _logEntries.InsertOne(logEntry);
            return logEntry;
        }

        public void Update(string id, LogEntry logEntryIn) =>
            _logEntries.ReplaceOne(logEntry => logEntry.Id == id, logEntryIn);

        public void Remove(LogEntry logEntryIn) =>
            _logEntries.DeleteOne(logEntry => logEntry.Id == logEntryIn.Id);

        public void Remove(string id)
        {
            _logEntries.DeleteOne(logEntry => logEntry.Id == id);
        }
    }
}
