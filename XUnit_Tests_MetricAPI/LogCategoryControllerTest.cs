﻿using System;
using Xunit;
using MetricAPI.Services;
using MetricAPI.Controllers;
using MetricAPI.Models;
using Microsoft.AspNetCore.Mvc;
//using XUnit_MetricAPI_Controller_Tests.FakeServices;
using AutoFixture;
using FakeItEasy;
using Microsoft.AspNetCore.Http;

namespace XUnit_Tests_MetricAPI
{
    public class LogCategoryServiceTest
    {
        //Fakes
        private readonly ILogCategoryService _service;

        //Dummy Data Generator
        private readonly Fixture _fixture;

        //System under test
        private readonly LogCategoryController _controller;
        public LogCategoryServiceTest()
        {
            _service = A.Fake<ILogCategoryService>();
            _controller = new LogCategoryController(_service);

            _fixture = new Fixture();
        }

        [Fact]
        public void Get_PassedValidId_ReturnsLogCategory()
        {
            // Arrange
            var logCategory = _fixture.Create<LogCategory>();
            var selector = logCategory.Id;
            A.CallTo(() => _service.Get(selector)).Returns(logCategory);

            // Act
            var logCategoryReturn = _controller.Get(selector);

            // Assert
            Assert.IsType<ActionResult<LogCategory>>(logCategoryReturn);
        }

        [Fact]
        public void Get_PassedInvalidId_ReturnsNotFound()
        {
            // Arrange
            var logCategory = _fixture.Create<LogCategory>();
            var selector = "rgb";
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var notFoundReturn = _controller.Get(selector).Result;

            // Assert
            Assert.IsType<NotFoundResult>(notFoundReturn);
        }

        [Fact]
        public void Create_PassedValidObject_ReturnsOkResult()
        {
            // Arrange
            var entry = _fixture.Create<LogCategory>();

            //Act
            var okResult = _controller.Create(entry).Result;

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void Update_PassedValidParams_ReturnsNoContent()
        {
            // Arrange
            var logCategory = _fixture.Create<LogCategory>();
            var selector = logCategory.Id;
            var newLogCategory = _fixture.Create<LogCategory>();
            A.CallTo(() => _service.Get(selector)).Returns(logCategory);

            // Act
            var noContentResult = _controller.Update(logCategory.Id, newLogCategory).Result;

            // Assert
            Assert.IsType<NoContentResult>(noContentResult);
        }

        [Fact]
        public void Update_PassedInvalidId_ReturnsNotFound()
        {
            // Arrange
            var logCategory = _fixture.Create<LogCategory>();
            var selector = "77";
            var newLogCategory = _fixture.Create<LogCategory>();
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var notFoundResult = _controller.Update(selector, newLogCategory).Result;

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void Delete_PassedValidId_ReturnsNoContent()
        {
            // Arrange
            var logCategory = _fixture.Create<LogCategory>();
            var selector = logCategory.Id;
            A.CallTo(() => _service.Get(selector)).Returns(logCategory);

            // Act
            var actionResult = _controller.Delete(selector);

            // Assert
            Assert.IsType<NoContentResult>(actionResult);
        }

        [Fact]
        public void Delete_PassedInvalidId_ReturnsNotFound()
        {
            // Arrange 
            var logCategory = _fixture.Create<LogCategory>();
            var selector = "77";
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var actionResult = _controller.Delete(selector);

            // Assert
            Assert.IsType<NotFoundResult>(actionResult);
        }
    }
}
