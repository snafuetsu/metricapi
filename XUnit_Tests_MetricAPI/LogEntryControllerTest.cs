using System;
using Xunit;
using MetricAPI.Services;
using MetricAPI.Controllers;
using MetricAPI.Models;
using AutoFixture;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;

namespace XUnit_Tests_MetricAPI
{
    public class LogEntryControllerTest
    {
        //Fakes
        private readonly ILogEntryService _service;

        //Dummy Data Generator
        private readonly Fixture _fixture;

        //System under test
        private readonly LogEntryController _controller;
        public LogEntryControllerTest()
        {
            _service = A.Fake<ILogEntryService>();
            _controller = new LogEntryController(_service);

            _fixture = new Fixture();
        }

        [Fact]
        public void Get_PassedValidId_ReturnslogEntry()
        {
            // Arrange
            var logEntry = _fixture.Create<LogEntry>();
            var selector = logEntry.Id;
            A.CallTo(() => _service.Get(selector)).Returns(logEntry);

            // Act
            var logEntryReturn = _controller.Get(selector);

            // Assert
            Assert.IsType<ActionResult<LogEntry>>(logEntryReturn);
        }

        [Fact]
        public void Get_PassedInvalidId_ReturnsNotFound()
        {
            // Arrange
            var logEntry = _fixture.Create<LogEntry>();
            var selector = "rgb";
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var notFoundReturn = _controller.Get(selector).Result;

            // Assert
            Assert.IsType<NotFoundResult>(notFoundReturn);
        }

        [Fact]
        public void Create_PassedValidObject_ReturnsOkResult()
        {
            // Arrange
            var entry = _fixture.Create<LogEntry>();

            //Act
            var okResult = _controller.Create(entry).Result;

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void Update_PassedValidParams_ReturnsNoContent()
        {
            // Arrange
            var logEntry = _fixture.Create<LogEntry>();
            var selector = logEntry.Id;
            var newLogEntry = _fixture.Create<LogEntry>();
            A.CallTo(() => _service.Get(selector)).Returns(logEntry);

            // Act
            var noContentResult = _controller.Update(logEntry.Id, newLogEntry).Result;

            // Assert
            Assert.IsType<NoContentResult>(noContentResult);
        }

        [Fact]
        public void Update_PassedInvalidId_ReturnsNotFound()
        {
            // Arrange
            var logEntry = _fixture.Create<LogEntry>();
            var selector = "77";
            var newLogEntry = _fixture.Create<LogEntry>();
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var notFoundResult = _controller.Update(selector, newLogEntry).Result;

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void Delete_PassedValidId_ReturnsNoContent()
        {
            // Arrange
            var logEntry = _fixture.Create<LogEntry>();
            var selector = logEntry.Id;
            A.CallTo(() => _service.Get(selector)).Returns(logEntry);

            // Act
            var actionResult = _controller.Delete(selector);

            // Assert
            //var noContentResult = actionResult as NoContentResult;
            Assert.IsType<NoContentResult>(actionResult);
        }

        [Fact]
        public void Delete_PassedInvalidId_ReturnsNotFound()
        {
            // Arrange 
            var logEntry = _fixture.Create<LogEntry>();
            var selector = "77";
            A.CallTo(() => _service.Get(selector)).Returns(null);

            // Act
            var actionResult = _controller.Delete(selector);

            // Assert
            Assert.IsType<NotFoundResult>(actionResult);
        }
    }

}
