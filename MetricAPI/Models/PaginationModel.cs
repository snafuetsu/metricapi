﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricAPI.Models
{
    public class PaginationModel
    {
        public int Page { get; set; }
        public int Pagesize { get; set; }
        public int PageTotalCount { get => (ResultsTotalCount - 1)/ Pagesize + 1; }
        public int ResultsTotalCount { get; set; }
        public dynamic Data { get; set; }
    }
}
