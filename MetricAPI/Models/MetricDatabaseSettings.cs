﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricAPI.Models
{
    public class MetricDatabaseSettings : IMetricDatabaseSettings
    {
        public string LogCategoriesCollectionName { get; set; }
        public string LogEntriesCollectionName { get; set; }
        public string LogProjectsCollectionName { get; set; }
        public string UserCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string Secret { get; set; }
    }

    public interface IMetricDatabaseSettings
    {
        string LogCategoriesCollectionName { get; set; }
        string LogProjectsCollectionName { get; set; }
        string LogEntriesCollectionName { get; set; }
        string UserCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string Secret { get; set; }
    }
}
