﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using MetricAPI.Models;
using MetricAPI.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MetricAPI.Controllers
{
    //[Authorize]
    [Route("api/Category")]
    public class LogCategoryController : ControllerBase
    {
        private readonly ILogCategoryService _logCategoryService;

        public LogCategoryController(ILogCategoryService logCategoryService)
        { 
            _logCategoryService = logCategoryService;
        }

        #region API Methods

        /// <summary>
        /// Get a paginated list of the categories under a certain project
        /// </summary>
        /// <param name="project">ID of project to look for categories under</param>
        /// <param name="page">Page number for Pagination</param>
        /// <param name="pagesize">Size of pages in number of items</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<PaginationModel> Get(string project, int page, int pagesize)
        {
            if (page < 1)
                page = 1;

            if (pagesize < 5 || pagesize > 50)
                pagesize = 10;

            var categories = _logCategoryService.Get(project, page, pagesize, out int totalresults);

            PaginationModel pagination = new PaginationModel()
            {
                Page = page,
                Pagesize = pagesize,
                ResultsTotalCount = totalresults,
                Data = categories
            };
            return pagination;
        }

        /// <summary>Returns a log category by ID</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        //[Authorize(Roles = "User,Admin")]
        [HttpGet("{id:length(24)}")]
        public ActionResult<LogCategory> Get(string id)
        {
            var logCategory = _logCategoryService.Get(id);

            if (logCategory == null)
            {
                return NotFound();
            }

            return logCategory;
        }


        /// <summary>Creates a new log cateogry</summary>
        /// <param name="item">The log cateogry item to be added.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult<LogCategory> Create(LogCategory item)
        {
            _logCategoryService.Create(item);

            return Ok(item);
        }

        /// <summary>Updates an existing log category</summary>
        /// <param name="id">The object identifier of the log category.</param>
        /// <param name="item">The new log category to update the item to.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpPut("{id:length(24)}")]
        public ActionResult<LogCategory> Update(string id, LogCategory item)
        {
            var logCategory = _logCategoryService.Get(id);

            if (logCategory == null)
            {
                return NotFound();
            }

            _logCategoryService.Update(id, item);

            return NoContent();
        }

        /// <summary>Deletes an existing log category</summary>
        /// <param name="id">The object identifier of the log category.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var logCategory = _logCategoryService.Get(id);

            if (logCategory == null)
            {
                return NotFound();
            }

            _logCategoryService.Remove(logCategory.Id);

            return NoContent();
        }
        #endregion API Methods
    }
}
