﻿using MetricAPI.Models;
using MetricAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricAPI.Controllers
{
    //[Authorize]
    [Route("api/Project")]
    public class LogProjectController : ControllerBase
    {
        private readonly ILogProjectService _logProjectService;

        public LogProjectController(ILogProjectService logProjectService)
        {
            _logProjectService = logProjectService;
        }

        #region API Methods
        /// <summary>
        /// Get a paginated list of all projects
        /// </summary>
        /// <param name="page">Page number for pagination</param>
        /// <param name="pagesize">Page size in number of items</param>
        /// <returns></returns>
        //[Authorize(Roles = "User,Admin")]
        [HttpGet]
        public ActionResult<PaginationModel> Get(int page, int pagesize)
        {
            if (page < 1)
                page = 1;

            if (pagesize < 5 || pagesize > 50)
                pagesize = 10;

            var projects = _logProjectService.Get(page, pagesize, out int totalresults);
            PaginationModel pagination = new PaginationModel()
            {
                Page = page,
                Pagesize = pagesize,
                ResultsTotalCount = totalresults,
                Data = projects
            };
            return pagination;
        }

        /// <summary>Returns a log project by ID</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        //[Authorize(Roles = "User,Admin")]
        [HttpGet("{id:length(24)}")]
        public ActionResult<LogProject> Get(string id)
        {
            var logProject = _logProjectService.Get(id);

            if (logProject == null)
            {
                return NotFound();
            }

            return logProject;
        }


        /// <summary>Creates a new log project</summary>
        /// <param name="item">The log project item to be added.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult<LogProject> Create(LogProject item)
        {
            _logProjectService.Create(item);

            return Ok(item);
        }

        /// <summary>Updates an existing log project</summary>
        /// <param name="id">The object identifier of the log project.</param>
        /// <param name="item">The new log project to update the item to.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpPut("{id:length(24)}")]
        public ActionResult<LogProject> Update(string id, LogProject item)
        {
            var logProject = _logProjectService.Get(id);

            if (logProject == null)
            {
                return NotFound();
            }

            _logProjectService.Update(id, item);

            return NoContent();
        }

        /// <summary>Deletes an existing log project</summary>
        /// <param name="id">The object identifier of the log project.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var logProject = _logProjectService.Get(id);

            if (logProject == null)
            {
                return NotFound();
            }

            _logProjectService.Remove(logProject.Id);

            return NoContent();
        }
        #endregion API Methods
    }
}
