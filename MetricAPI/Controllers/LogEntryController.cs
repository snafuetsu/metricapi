﻿using System;
using MetricAPI.Models;
using MetricAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace MetricAPI.Controllers {
    //[Authorize]
    [Route("api/Entry")]
    
    public class LogEntryController : ControllerBase
    {
        private readonly ILogEntryService _logEntryService;

        /// <summary>Initializes a new instance of the <see cref="LogEntryController"/> class.</summary>
        /// <param name="logEntryService"></param>
        public LogEntryController(ILogEntryService logEntryService)
        {
            // Dependency Injection for the Database Context
            _logEntryService = logEntryService;
        }

        #region API Methods


        [HttpGet]
        public ActionResult<PaginationModel> Get(string category, int page, int pagesize, DateTime? start, DateTime? end)
        {
            if (page < 1)
                page = 1;

            if (pagesize < 5 || pagesize > 50)
                pagesize = 10;

            var entries = _logEntryService.Get(category, page, pagesize, start, end, out int totalresults);
            PaginationModel pagination = new PaginationModel()
            {
                Page = page,
                Pagesize = pagesize,
                ResultsTotalCount = totalresults,
                Data = entries
            };
            return pagination;
        }

        /// <summary>Returns a log entry by ID</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        //[Authorize(Roles = "User,Admin")]
        [HttpGet("{id:length(24)}")]
        public ActionResult<LogEntry> Get(string id)
        {
            var logEntry = _logEntryService.Get(id);

            if (logEntry == null)
            {
                return NotFound();
            }

            return logEntry;
        }

        /// <summary>Creates a new log entry</summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpPost]
        public ActionResult<LogEntry> Create(LogEntry item)
        {
            _logEntryService.Create(item);

            return Ok(item);
        }

        /// <summary>Updates an existing log entry</summary>
        /// <param name="id">The identifier.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpPut("{id:length(24)}")]
        public ActionResult<LogEntry> Update(string id, LogEntry item)
        {
            var logEntry = _logEntryService.Get(id);

            if (logEntry == null)
            {
                return NotFound();
            }

            _logEntryService.Update(id, item);

            return NoContent();
        }

        /// <summary>Deletes an existing log entry</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        //[Authorize(Roles = Role.Admin)]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            // Find metric entry in the collection in the database
            var logEntry = _logEntryService.Get(id);

            // If not found, return NotFound()
            if (logEntry == null)
            {
                return NotFound();
            }

            // Remove the Metric entry from the DB
            _logEntryService.Remove(logEntry.Id);

            // No content is returned
            return NoContent();
        }
        #endregion API Methods
    }
}