﻿using System.Collections.Generic;
using MetricAPI.Models;

namespace MetricAPI.Services
{
    public interface ILogProjectService
    {
        LogProject Create(LogProject logProject);
        List<LogProject> Get(int page, int pagesize, out int totalresults);
        LogProject Get(string id);
        void Remove(LogProject logProjectIn);
        void Remove(string id);
        void Update(string id, LogProject logProjectIn);
    }
}