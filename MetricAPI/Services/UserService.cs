﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MetricAPI.Models;
using MetricAPI.Helpers;
using MongoDB.Driver;

namespace MetricAPI.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(string id);
        User Create(User user);
        void Update(string id, User userIn);
    }

    public class UserService : IUserService
    {
        // hardcoded for testing, change to use db in production
        //private List<User> _users = new List<User>
        //{
        //    new User { Id = 1, Username = "admin", Password = "admin", Role = Role.Admin},
        //    new User { Id = 2, Username = "user", Password = "user", Role = Role.User},
        //    new User { Id = 3, Username = "sysadmin", Password = "sysadmin", Role = Role.SysAdmin}
        //};

        private readonly IMongoCollection<User> _users;
        private readonly string _secret;

        public UserService(IMetricDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.UserCollectionName);
            _secret = settings.Secret;
            
        }

        //private readonly AppSettings _appSettings;

        //public UserService(IOptions<AppSettings> appSettings)
        //{
        //    _appSettings = appSettings.Value;
        //}

        public User Authenticate(string username, string password)
        {
            //var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);
            var user = _users.Find<User>(x => x.Username == username && x.Password == password).SingleOrDefault();

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful, so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            // return users without passwords
            //return _users.Select(x =>
            //{
            //    x.Password = null;
            //    return x;
            //});

            return _users.Find(user => true).ToList().Select(x =>
            {
                x.Password = null;
                return x;
            });
        }

        public User GetById(string id)
        {
            //var user = _users.FirstOrDefault(x => x.Id == id);
            var user = _users.Find<User>(x => x.Id == id).FirstOrDefault();

            // return user without password
            if (user != null)
            {
                user.Password = null;
            }

            return user;
        }

        public User Create(User user)
        {
            _users.InsertOne(user);
            return user;
        }

        public void Update(string id, User userIn) =>
          _users.ReplaceOne(logEntry => logEntry.Id == id, userIn);
    }
}
