﻿using MetricAPI.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricAPI.Services
{
    public class LogEntryService : ILogEntryService
    {
        private readonly IMongoCollection<LogEntry> _logEntries;
        private readonly IMongoCollection<LogCategory> _logCategories;

        public LogEntryService(IMetricDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _logEntries = database.GetCollection<LogEntry>(settings.LogEntriesCollectionName);
            _logCategories = database.GetCollection<LogCategory>(settings.LogCategoriesCollectionName);
        }

        public List<LogEntry> Get(string categoryid, int page, int pagesize, DateTime? start, DateTime? end, out int totalresults)
        {
            var query = from e in _logEntries.AsQueryable<LogEntry>()
                        where e.Category == categoryid
                        select e;

            if (start != null)
                query = from e in query
                        where e.Timestamp >= start
                        select e;

            if (end != null)
                query = from e in query
                        where e.Timestamp <= end
                        select e;

            totalresults = query.Count();

            return query.Skip((page - 1) * pagesize).Take(pagesize).ToList<LogEntry>();
        }

        public LogEntry Get(string id) =>
            _logEntries.Find<LogEntry>(logEntry => logEntry.Id == id).FirstOrDefault();

        public LogEntry Create(LogEntry logEntry)
        {
            _logEntries.InsertOne(logEntry);
            return logEntry;
        }

        public void Update(string id, LogEntry logEntryIn) =>
            _logEntries.ReplaceOne(logEntry => logEntry.Id == id, logEntryIn);

        public void Remove(LogEntry logEntryIn) =>
            _logEntries.DeleteOne(logEntry => logEntry.Id == logEntryIn.Id);

        public void Remove(string id)
        {
            _logEntries.DeleteOne(logEntry => logEntry.Id == id);
        }
    }
}
