﻿using MetricAPI.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricAPI.Services
{
    public class LogProjectService : ILogProjectService
    {
        private readonly IMongoCollection<LogProject> _logProjects;

        public LogProjectService(IMetricDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _logProjects = database.GetCollection<LogProject>(settings.LogProjectsCollectionName);
        }

        public List<LogProject> Get(int page, int pagesize, out int totalresults)
        {
            var fluentfind = _logProjects.Find(logProject => true);
            totalresults = (int)fluentfind.CountDocuments();
            return fluentfind
                    .Skip((page - 1) * pagesize)
                    .Limit(pagesize)
                    .ToList();
        }

        public LogProject Get(string id) =>
            _logProjects.Find<LogProject>(logProject => logProject.Id == id).FirstOrDefault();

        public LogProject Create(LogProject logProject)
        {
            _logProjects.InsertOne(logProject);
            return logProject;
        }

        public void Update(string id, LogProject logProjectIn)
        {
            _logProjects.ReplaceOne(logProject => logProject.Id == id, logProjectIn);
        }

        public void Remove(LogProject logProjectIn)
        {
            _logProjects.DeleteOne(logProject => logProject.Id == logProjectIn.Id);
        }

        public void Remove(string id)
        {
            _logProjects.DeleteOne(logProject => logProject.Id == id);
        }
    }
}
