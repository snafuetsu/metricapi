﻿using System;
using System.Collections.Generic;
using MetricAPI.Models;

namespace MetricAPI.Services
{
    public interface ILogEntryService
    {
        LogEntry Create(LogEntry logEntry);
        LogEntry Get(string id);
        List<LogEntry> Get(string categoryid, int page, int pagesize, DateTime? start, DateTime? end, out int totalresults);
        void Remove(LogEntry logEntryIn);
        void Remove(string id);
        void Update(string id, LogEntry logEntryIn);
    }
}