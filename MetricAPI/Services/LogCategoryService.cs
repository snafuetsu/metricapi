﻿using MetricAPI.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricAPI.Services
{
    public class LogCategoryService : ILogCategoryService
    {
        private readonly IMongoCollection<LogCategory> _logCategories;

        public LogCategoryService(IMetricDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _logCategories = database.GetCollection<LogCategory>(settings.LogCategoriesCollectionName);
        }

        public List<LogCategory> Get(string projectid, int page, int pagesize, out int totalresults)
        {
            var query = from c in _logCategories.AsQueryable<LogCategory>()
                        where c.Project == projectid
                        select c;

            totalresults = query.Count();
            return query.Skip((page - 1) * pagesize).Take(pagesize).ToList();
        }

        public LogCategory Get(string id) =>
            _logCategories.Find<LogCategory>(logCategory => logCategory.Id == id).FirstOrDefault();

        public LogCategory Create(LogCategory logCategory)
        {
            _logCategories.InsertOne(logCategory);
            return logCategory;
        }

        public void Update(string id, LogCategory logCategoryIn)
        {
            _logCategories.ReplaceOne(logCategory => logCategory.Id == id, logCategoryIn);
        }

        public void Remove(LogCategory logCategoryIn)
        {
            _logCategories.DeleteOne(logCategory => logCategory.Id == logCategoryIn.Id);
        }

        public void Remove(string id)
        {
            _logCategories.DeleteOne(logCategory => logCategory.Id == id);
        }
    }
}
