﻿using System.Collections.Generic;
using MetricAPI.Models;

namespace MetricAPI.Services
{
    public interface ILogCategoryService
    {
        LogCategory Create(LogCategory logCategory);
        LogCategory Get(string id);
        List<LogCategory> Get(string projectid, int page, int pagesize, out int totalresults);
        void Remove(LogCategory logCategoryIn);
        void Remove(string id);
        void Update(string id, LogCategory logCategoryIn);
    }
}